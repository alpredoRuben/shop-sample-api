require('dotenv').config();
const jwt = require('jsonwebtoken');
const verifyToken = (req, res, next) => {
    const authHeader = req.headers.token;

    if (!authHeader) {
        return res.status(401).json({
            message: 'You are not authenticated!',
        });
    }

    const token = authHeader.split(' ')[1];

    console.log('REQUEST HEADERS', req.headers.token);

    jwt.verify(token, process.env.JWT_SECRET_KEY, (err, data) => {
        if (err) {
            res.status(403).json({
                message: 'Token is not valid',
                errrors: err,
            });
        }

        req.user = data;
        next();
    });
};

const verifyTokenAndAuthorization = (req, res, next) => {
    verifyToken(req, res, () => {
        if (req.user.id === req.params.id || req.user.isAdmin) {
            next();
        } else {
            res.status(403).json({
                message: 'You are not allowed to access that service',
            });
        }
    });
};

const verifyTokenAndAdmin = (req, res, next) => {
    verifyToken(req, res, () => {
        if (req.user.isAdmin) {
            next();
        } else {
            res.status(403).json({
                message: 'You are not allowed to access that service',
            });
        }
    });
};

module.exports = {
    verifyToken,
    verifyTokenAndAuthorization,
    verifyTokenAndAdmin,
};
