const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const port = process.env.PORT || 5000;

/** Import Route */
const authRoute = require('./routes/authRoute');
const userRoute = require('./routes/userRoute');
const productRoute = require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute');
const cartRoute = require('./routes/cartRoute');

dotenv.config();

mongoose
    .connect(process.env.MONGODB_URL)
    .then(() => console.log('DB Connection Success'))
    .catch((err) => console.log(err));

app.use(cors());
app.use(express.json());

/** Set Route */
app.use('/api/auth', authRoute);
app.use('/api/user', userRoute);
app.use('/api/product', productRoute);
app.use('/api/order', orderRoute);
app.use('/api/cart', cartRoute);

app.listen(port, () => {
    console.log(`Running on server at port ${port}`);
});
