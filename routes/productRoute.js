const router = require('express').Router();
const Product = require('../models/Product');
const {
    verifyToken,
    verifyTokenAndAdmin,
    verifyTokenAndAuthorization,
} = require('../middlewares/verifyTokenMiddleware');

/** Route Find Product */
router.get('/find/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id);
        return res.status(200).json({
            results: product,
        });
    } catch (err) {
        return res.status(500).json({
            errors: err,
        });
    }
});

/** Route Get All Product */
router.get('/', async (req, res) => {
    const query_new = req.query.new;
    const query_category = req.query.category;

    try {
        let products;

        if (query_new) {
            products = await Product.find().sort({ createdAt: -1 }).limit(1);
        } else if (query_category) {
            products = await Product.find({
                categories: {
                    $in: [query_category],
                },
            });
        } else {
            products = await Product.find();
        }

        return res.status(200).json({ results: products });
    } catch (err) {
        return res.status(500).json({
            errors: err,
        });
    }
});

/** Route Store Product */
router.post('/', verifyTokenAndAdmin, async (req, res) => {
    const new_product = new Product(req.body);

    try {
        const save_product = await new_product.save();
        return res.status(201).json({
            message: 'Product created successfully',
            results: save_product,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to create product',
            errors: err,
        });
    }
});

/** Route Update Product */
router.put('/:id', verifyTokenAndAdmin, async (req, res) => {
    try {
        const update_product = await Product.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );

        return res.status(200).json({
            message: 'Product updated successfully',
            results: update_product,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to update product',
            errors: err,
        });
    }
});

/** Route Delete Product */
router.delete('/:id', verifyTokenAndAdmin, async (req, res) => {
    try {
        await Product.findByIdAndDelete(req.params.id);

        return res.status(200).json({
            message: 'Product deleted successfully',
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to delete product',
            errors: err,
        });
    }
});

module.exports = router;
