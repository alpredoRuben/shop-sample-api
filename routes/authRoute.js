const router = require('express').Router();
const User = require('../models/User');
const CryptoJS = require('crypto-js');
const jwt = require('jsonwebtoken');

/** Register */
router.post('/register', async (req, res) => {
    try {
        const new_user = new User({
            name: req.body.fullname,
            username: req.body.username,
            email: req.body.email,
            password: CryptoJS.AES.encrypt(
                req.body.password,
                process.env.SECRET_KEY
            ).toString(),
        });

        const save_user = await new_user.save();

        return res.status(201).json({
            message: `user ${save_user.username} success registered`,
            results: save_user,
        });
    } catch (err) {
        console.log('ERROR SAVE USER', err);
        return res.status(500).json({
            message: 'Failed to registered user',
            errors: err,
        });
    }
});

/** Login */
router.post('/login', async (req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username });
        !user &&
            res
                .status(401)
                .json({ message: 'Unauthorized. Wrong Credentials' });

        const password_hash = CryptoJS.AES.decrypt(
            user.password,
            process.env.SECRET_KEY
        );
        const original_password = password_hash.toString(CryptoJS.enc.Utf8);

        original_password != req.body.password &&
            res.status(401).json({ message: 'Password does not match' });

        const access_token = jwt.sign(
            {
                id: user._id,
                isAdmin: user.isAdmin,
            },
            process.env.JWT_SECRET_KEY,
            { expiresIn: '3d' }
        );

        const { password, ...others } = user._doc;

        res.status(200).json({
            message: 'Your credential valid. Login successfully',
            results: { ...others, token: access_token },
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed login',
            errors: err,
        });
    }
});

module.exports = router;
