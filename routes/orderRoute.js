const router = require('express').Router();
const Order = require('../models/Order');
const {
    verifyToken,
    verifyTokenAndAdmin,
    verifyTokenAndAuthorization,
} = require('../middlewares/verifyTokenMiddleware');

/** Route Find Order */
router.get('/find/:userId', verifyTokenAndAuthorization, async (req, res) => {
    try {
        const orders = await Order.find({ userId: req.params.userId });
        return res.status(200).json({ results: orders });
    } catch (err) {
        return res.status(500).json({ errors: err });
    }
});

/** Route Get All Order */
router.get('/', verifyTokenAndAdmin, async (req, res) => {
    try {
        const orders = await Order.find();
        return res.status(200).json({ results: orders });
    } catch (err) {
        return res.status(500).json({ errors: err });
    }
});

/** Router Get Income */
router.get('/income', verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const last_month = new Date(date.setMonth(date.getMonth() - 1));
    const previous_month = new Date(
        new Date().setMonth(last_month.getMonth() - 1)
    );

    try {
        const income = await Order.aggregate([
            { $match: { createdAt: { $gte: previous_month } } },
            {
                $project: {
                    month: { $month: '$createdAt' },
                    sales: '$amount',
                },
            },
            {
                $group: {
                    _id: '$month',
                    total: { $sum: '$sales' },
                },
            },
        ]);
        return res.status(200).json({
            resutls: income,
        });
    } catch (err) {
        return res.status(500).json({
            errors: err,
        });
    }
});

/** Route Store Order */
router.post('/', verifyToken, async (req, res) => {
    const new_order = new Order(req.body);

    try {
        const save_order = await new_order.save();
        return res.status(201).json({
            message: 'Your ordering created successfully',
            results: save_order,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to create your ordering',
            errors: err,
        });
    }
});

/** Route Update Order */
router.put('/:id', verifyTokenAndAdmin, async (req, res) => {
    try {
        const update_order = await Order.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );

        return res.status(200).json({
            message: 'Your ordering updated successfully',
            results: update_order,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to update your ordering',
            errors: err,
        });
    }
});

/** Route Delete Order */
router.delete('/:id', verifyTokenAndAdmin, async (req, res) => {
    try {
        await Order.findByIdAndDelete(req.params.id);
        return res.status(200).json({
            message: 'Your ordering has been deleted...',
        });
    } catch (err) {
        return res.status(500).json({ errors: err });
    }
});

module.exports = router;
