const router = require('express').Router();
const Cart = require('../models/Cart');
const {
    verifyToken,
    verifyTokenAndAdmin,
    verifyTokenAndAuthorization,
} = require('../middlewares/verifyTokenMiddleware');

/** Route Find Cart */
router.get('/find/:id', verifyTokenAndAuthorization, async (req, res) => {
    try {
        const cart = await Cart.findOne({ userId: req.params.userId });
        return res.status(200).json({
            results: cart,
        });
    } catch (err) {
        return res.status(500).json({
            errors: err,
        });
    }
});

/** Route Get All Cart */
router.get('/', verifyTokenAndAdmin, async (req, res) => {
    try {
        const carts = await Cart.find();
        return res.status(200).json({ results: carts });
    } catch (err) {
        return res.status(500).json({ errors: err });
    }
});

/** Route Store Cart */
router.post('/', verifyToken, async (req, res) => {
    const new_cart = new Cart(req.body);
    try {
        const save_cart = await new_cart.save();
        return res.status(201).json({
            message: 'Cart success created',
            results: save_cart,
        });
    } catch (err) {
        return res
            .status(500)
            .json({ message: 'Failed to create cart', errors: err });
    }
});

/** Route Update Cart */
router.put('/:id', verifyTokenAndAuthorization, async (req, res) => {
    try {
        const update_cart = await Cart.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );

        return res.status(200).json({
            message: 'Cart updated successfully',
            results: update_cart,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Failed to update cart',
            errors: err,
        });
    }
});

/** Route Delete Cart */
router.delete('/:id', verifyTokenAndAuthorization, async (req, res) => {
    try {
        await Cart.findByIdAndDelete(req.params.id);
        return res.status(200).json({ message: 'Cart has been deleted...' });
    } catch (err) {
        return res.status(500).json({ errors: err });
    }
});

module.exports = router;
