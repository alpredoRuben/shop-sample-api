const router = require('express').Router();
const CryptoJS = require('crypto-js');

/** Import Models */
const User = require('../models/User');

/** Import Middleware */
const {
    verifyTokenAndAuthorization,
    verifyTokenAndAdmin,
} = require('../middlewares/verifyTokenMiddleware');

/** Route Update User*/
router.put('/:id', verifyTokenAndAuthorization, async (req, res) => {
    if (req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(
            req.body.password,
            process.env.SECRET_KEY
        ).toString();
    }

    try {
        const update_user = await User.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );

        return res.status(200).json({
            message: 'User updated successfully',
            results: update_user,
        });
    } catch (err) {
        return res.status(500).json({
            message: 'User failed to update',
            errors: err,
        });
    }
});

/** Route Delete User */
router.delete('/:id', verifyTokenAndAuthorization, async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id);
        return res.status(200).json({
            message: 'User deleted successfully',
        });
    } catch (err) {
        return res.status(500).json({
            message: 'Deleted user fail',
            errors: err,
        });
    }
});

/** Route Find User */
router.get('/find/:id', verifyTokenAndAdmin, async (req, res) => {
    try {
        const find_user = await User.findById(req.params.id);
        const { password, ...others } = find_user._doc;

        return res.status(200).json({ ...others });
    } catch (err) {
        return res.status(500).json({
            message: 'User not ready and not found',
            errors: err,
        });
    }
});

/** Route Get All User */
router.get('/', verifyTokenAndAdmin, async (req, res) => {
    const query = req.query.new;
    try {
        const users = query
            ? await User.find().sort({ _id: -1 }).limit(5)
            : await User.find();
        return res.status(200).json(users);
    } catch (err) {
        return res.status(500).json({
            message: 'Failed get user',
            errors: err,
        });
    }
});

/** Route Get User Statistics */
router.get('/statistics', verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const last_year = new Date(date.setFullYear(date.getFullYear() - 1));

    try {
        const data = await User.aggregate([
            { $match: { createdAt: { $gte: last_year } } },
            {
                $project: {
                    month: { $month: '$createdAt' },
                },
            },
            {
                $group: {
                    _id: '$month',
                    total: { $sum: 1 },
                },
            },
        ]);

        return res.status(200).json({
            results: data,
        });
    } catch (err) {
        res.status(500).json({
            message: 'Failed',
            errors: err,
        });
    }
});

module.exports = router;
